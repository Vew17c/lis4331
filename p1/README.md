> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advance Mobile Application Development

## Vincent Williams

### Project 1 Requirements:

1. Create Splash Screen
2. Include artist media and image
3. Buttons and Images must be horizontally alligned
4. Create launcher icon

README.md file should include the following items:        

* Screenshot of running app's splash screen
* Screenshot of running follow up screen
* Screenshot of play and pause
                                                                                                                           

|Assignment Screenshots: | | 
|:-- |:-- |:--
| *Screenshot of Splash Screen*: | *Main Activity - Showing all the songs*: | *Song Playing*:
| | | 
| ![Spalsh Screen](../img/p1_1.png) | ![App Main Activity](../img/p1_2.png) | ![Song Playing](../img/p1_3.png)
| | | 

