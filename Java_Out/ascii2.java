import java.util.Scanner;

public class ascii2 {
    public static void main (String[] args){
        int valueN = 0;
        boolean lock = true;
        String inputNumber = "";

        Scanner input = new Scanner(System.in);

        System.out.println("Printing letters A - Z as ASCII values"); 

        for ( valueN = 65; valueN <= 90; valueN++){
            System.out.println("Character " + (char)valueN + " has an ASCII value of " + valueN);
        }

        System.out.println("\nPrinting ASCII values of 48 - 122 as characters");

        for ( valueN = 48; valueN <= 122; valueN++){
            System.out.println("ASCII value " + valueN + " has a character value of " + (char)valueN);
        }

        System.out.print("\nAllowing user ASCII value input:\nPlease enter ASCII value (32 - 127): ");
        
        while(lock == true){

            inputNumber = input.next();

            try {
               valueN = Integer.parseInt(inputNumber); 
               lock = false;

               if (valueN > 127 || valueN < 32){
                System.out.print("ASCII value must be >= 32 and <= 127.\n\nPlease enter ASCII value (32 - 127): ");
                lock = true;
               }
            } catch (Exception e) {
                System.out.print("Invalid integer--ASCII value must be a number.\n\nPlease enter ASCII value (32 - 127): ");
                lock = true;
            }
            }

            System.out.print("\nASCII value " + valueN + " has character value " + (char)valueN );
        }
    }