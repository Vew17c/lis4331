import java.util.Scanner;
import java.text.DecimalFormat;

class Book_Demo {
    public static void main(String[] args){
        
        String codeI = "",
               descriptI = "",
               priceI = "",
               authorI = "";
        
        Scanner input = new Scanner(System.in);
        DecimalFormat f = new DecimalFormat("###,###.##");

        //System.out.println("/// Below are the user-entered values: /////");

        Product pro = new Product();

        System.out.println("Code = " + pro.getCode());
        System.out.println("Description = " + pro.getDescription());
        System.out.println("Price = $" + pro.getPrice());

        System.out.println("\n///// Blow are user-entered values: ////");

        System.out.print("Code: ");
        codeI = input.next();
        System.out.print("Description: ");
        input.nextLine();
        descriptI = input.nextLine(); 
        System.out.print("Price: ");
        priceI = input.next();

        Product pro2 = new Product(codeI, descriptI, priceI);

        System.out.println("Code = " + pro2.getCode());
        System.out.println("Description = " + pro2.getDescription());
        System.out.println("Price = $" + pro2.getPrice());

        System.out.println("\n//// Below using setter methoods");
        pro2.setCode("xyz789");
        pro2.setDescription("Test Widget");
        pro2.setPrice("89.99");
        pro2.print();

        //System.out.println("/// Below are the user-entered values: /////");

        //Product aut = new Product();
        Author aut = new Author();

        System.out.println("Code = " + aut.getCode());
        System.out.println("Description = " + aut.getDescription());
        System.out.println("Price = $" + aut.getPrice());
        System.out.println("Auhtor = " + aut.getAuthor());

        System.out.println("\n///// Blow are user-entered values: ////");

        System.out.print("Code: ");
        codeI = input.next();
        System.out.print("Description: ");
        input.nextLine();
        descriptI = input.nextLine();
        System.out.print("Price: ");
        priceI = input.next();
        System.out.print("Author: ");
        authorI = input.next();

        Author aut2 = new Author(codeI, descriptI, priceI, authorI);

        System.out.println("Code = " + aut2.getCode());
        System.out.println("Description = " + aut2.getDescription());
        System.out.println("Price = $" + aut2.getPrice());
        System.out.println("Auhtor = " + aut2.getAuthor());

        System.out.println("\n//// Below using setter methoods");
        aut2.print();

    }
}