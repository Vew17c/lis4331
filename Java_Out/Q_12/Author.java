class Author extends Product {
    private String name ;

    public Author(){
        super();
        System.out.println("\nInside author default constructors.");
        name = "Terry";
    }

    public Author(String code, String description, String price, String name){
        super(code, description, price);
        System.out.println("\nInside author constructor with parameters.");
        this.name = name;
    }

    public String getAuthor(){
        return name;
    }

    public void setAuthor(String n){
        name = n;
    }

    public void print() {
        super.print();
        System.out.print(" Author: " + name);
    }
}