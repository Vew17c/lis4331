import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class SelectNCopy implements ActionListener
{
	public static void main (String [] args){
		SelectNCopy gui = new SelectNCopy();
	}
		private JFrame frame;
		private JTextArea list1;
		private JTextArea list2;
        private JButton copyB;
        
        String[] arr21 = { "Blue" , "Green" , "Red" , "Orange" , "Yellow" , "Teal" , "Black" , "White" , "Cyan" , "Purple"};
        String arr1 = "Test";
		
		public SelectNCopy()
		{
			list1 = new JTextArea(arr1,5,1);
			list2 = new JTextArea(5,1);
            copyB = new JButton("Copy");
        
            copyB.addActionListener(this);

			JFrame frame = new JFrame("Multiple Selection List");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            
			frame.setLayout(new BorderLayout(5, 3));
			frame.add(list1, BorderLayout.LINE_START);
			frame.add(list2, BorderLayout.LINE_END);
			frame.add(copyB, BorderLayout.CENTER);
			frame.pack();
			frame.setVisible(true);
        }
        
        public void actionPerformed(ActionEvent event)
	{ }
		
/*
	public void actionPerformed(ActionEvent event)
	{
		String heightText = heightField.getText();
		double height = Double.parseDouble(heightText);
		String weightText = weightField.getText();
		double weight = Double.parseDouble(weightText);
		
        double bmi = (height * height) + ( weight * weight);
        bmi = Math.sqrt(bmi);
		bmiLabel.setText(String.format("Leg C: %.2f" , bmi));
    }
    */
}