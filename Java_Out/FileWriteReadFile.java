import java.io.FileWriter;
import java.io.File;
import java.util.Scanner;

public class FileWriteReadFile {
    public static void main (String[] args) throws Exception {
        int wordNum = 0;
        String inputText = "";
        File file = new File("filecountwords.txt");
        FileWriter fr = null;
        Scanner input = new Scanner(System.in);
        Scanner sc = new Scanner(file);

        System.out.print("\nPlease enter text: ");
        inputText = input.nextLine();
    
        // Writes the file
        try {
            fr = new FileWriter(file);
            fr.write(inputText);
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            //close resources
            try {
                fr.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // Prints out the results
        System.out.println("Saved to file filecountwords.txt");
        System.out.println("\nNow reading file filecountwords.txt");

        /*
        
        wordNum = countWordsUsingSplit(inputText);

        System.out.print("Number of words: " + wordNum + "\n"); */

        while (sc.hasNextLine()) 
      System.out.println(sc.nextLine());

        
    }


    /*
    // Counts the number of words using a split methood.
    public static int countWordsUsingSplit(String input) { 
        if (input == null || input.isEmpty()) { 
            return 0; 
        } 
        
        String[] words = input.split("\\s+"); 
        
        return words.length; }*/
}