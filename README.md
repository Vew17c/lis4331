> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advance Mobile Applications Development

## Vincent Williams

### LIS4331 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")

    - Install Java
    - Install Android Studios
    - Creating "My First App" in Android Studios
    - Setting up Bitbucket
    - Creating "MyBuisnessCard" in Android Studios
    - Defining Git Commands

2. [A2 README.md](a2/README.md "My A2 README.md file")

    - Researching Spinner Functions
    - Program basic arithemitic into an application
    - Learn how to change background color
    - Learn how to dispaly and change the launcher icon image

3. [A3 README.md](a3/README.md "My A3 README.md file")

    - Learn how to use a RadioBUtton and a RadioGroup
    - Learn how to use toast messages to display results and errors
    - Creating a splash screen 

4. [A4 README.md](a4/README.md "My A4 README.md file")

    - Learn about what Shared Preferences is and how to use them
    - Using Shared Preferences for multiple of activties
    - Create a Mortgage Intrest Calculator in Android Studios
    - Learning how to change text and image in other activities while the progrma is running
    
5. [A5 README.md](a5/README.md "My A5 README.md file")

    - Learned about what an RSS feed is
    - Learned about how to create multiple of classes
    - Demonstrated use of ListView
    - Demonstrated use of ArrayMaps

2. [P1 README.md](p1/README.md "My P1 README.md file")

    - Create a splash screen
    - Learn how to use MediaPlayer
    - Create an app that plays music
    - Learn how to show and hide Views

2. [P2 README.md](p2/README.md "My P2 README.md file")

    - Create a task list
    - Learn how the database classes work in java
    - Create a database for your app to use.



