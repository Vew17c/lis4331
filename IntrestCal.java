import java.util.Scanner;

public class IntrestCal {

    public static void main (String[] args){
        int year = 0;

        double principleNumb = 0.0,
               testYear = 0.0,
               testIntrest = 0.0,
               results = 0.0;

        final double RATE = 3.35;

        boolean lock = true;

        String line = "";

        Scanner input = new Scanner(System.in);

        System.out.print("\nNote: Program checks for integers for years, and non-numeric values. \n\n");

        //Grabs the input for number 2. Using Try Catch to catch an user input a letter value (which is invalid).
        while (lock == true) { 
            System.out.print("Enter principal: $");
            line = input.next();
        try {
            principleNumb = Double.parseDouble(line);
            lock = false;
        } catch (Exception e) {
            System.out.print("Not a number. Please try again.\n\n");
            lock = true;
        }
        }

        lock = true;
        System.out.print("\n");
        while (lock == true) { 
            System.out.print("Enter Intrest: $");
            line = input.next();
        try {
            testIntrest = Double.parseDouble(line);
            lock = false;
        } catch (Exception e) {
            System.out.print("Not a number. Please try again.\n\n");
            lock = true;
        }
        }

        //Grabs the input for number 2. Using Try Catch to catch an user input a letter value (which is invalid).
        lock = true;
        System.out.print("\n");
        while (lock == true) { 
            System.out.print("Time (per year): ");
            line = input.next();
        try {
            testYear = Double.parseDouble(line);
            lock = false;
            if (testYear%1 != 0) {
                System.out.print("Not a valid integer\n\n");
                System.out.print("Please try again. ");
                lock = true;
            } else {
                year = (int)testYear;
            }
        } catch (Exception e) {
            System.out.print("Not a number. Please try again.\n\n");
            lock = true;
        }
        }

        testIntrest = testIntrest / 100;

        results = ((( principleNumb * testIntrest) * year ) + principleNumb );
        
        // ( principle * intrest ) -> * years -> + principle.

        testIntrest = testIntrest * 100;

        //Prints to the console telling you what numbers are stored in numb1 and numb2 before the switch
        System.out.printf("\nYou will have save $%.2f" , results);
        System.out.print(" in " + year + " years, at an intrest rate of " + testIntrest + "%.");

    }
}